# Use the official Python image from the Docker Hub
FROM python:3.9

# Make a directory in our Docker image where our app will be placed
RUN mkdir /app

# Set "app" as the working directory
WORKDIR /app

# Copy the currency.py file into our app directory
COPY currency.py /app

# Copy the requirements.txt file into our app directory
COPY requirements.txt /app

# Execute pip in the context of the container to install our dependencies
RUN pip install --no-cache-dir -r requirements.txt

# We'll run the app in the output, listening on port 5000
CMD ["python", "/app/currency.py"]
