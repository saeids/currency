"""
Currency Converter
"""

# Import the requests library for making HTTP requests
import requests

def convert_currency():
    try:
        # Print a welcome message to the user
        print("\nWelcome to the Currency Converter!\n")
        
        # Collect user input for the source currency, target currency, and amount to convert
        from_currency = str(input("Enter the currency you'd like to convert from (e.g., USD): ")).upper()
        to_currency = str(input("Enter the currency you'd like to convert to (e.g., EUR): ")).upper()
        amount = float(input("Enter the amount of money: "))
        
        # Make a request to the currency conversion API with the user's input
        response = requests.get(
            f"https://api.frankfurter.app/latest?amount={amount}"
            f"&from={from_currency}&to={to_currency}",
            timeout=10
        )
        # Raise an exception if the HTTP request returned an error status
        response.raise_for_status()  
    except requests.exceptions.RequestException as e:
        # Handle any errors that occurred during the HTTP request
        print("\nAn error occurred while communicating with the currency conversion API.")
        print("Please check your input and try again.")
        print(f"Error details: {e}")
    except ValueError as e:
        # Handle any errors that occurred while processing the user's input
        print("\nAn error occurred while processing your input.")
        print("Please ensure you entered valid currency codes and a numerical amount.")
        print(f"Error details: {e}")
    else:
        # If no exceptions were raised, print the result of the currency conversion
        print(
            f"\n{amount} {from_currency} is {response.json()['rates'][to_currency]}"
            f" {to_currency}\n"
        )

# Run the convert_currency function when this script is executed
if __name__ == '__main__':
    convert_currency()
