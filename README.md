# Currency Converter

Currency Converter is a Python application that allows you to convert currencies using various API services. It supports conversion between currencies such as USD, EUR, GBP, and SEK.

## Features

- Convert currencies using Exchangerate-api, Currencyconverter-api, and Open exchange rates API
- Written in Python
- Integration with GitLab for code hosting and pipelines

## API Keys

To access the API services, you will need to obtain API keys from the following providers:

- Exchangerate-api: [API Key Registration](https://exchangerate-api.com/)
- Currencyconverter-api: [API Key Registration](https://www.currencyconverterapi.com/)
- Open exchange rates: [API Key Registration](https://openexchangerates.org/)

## Installation

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/saeids/currency.git
   ```

2. Create a virtual environment and activate it:

   ```bash
   python -m venv venv
   source venv/bin/activate
   ```

3. Install the dependencies:

   ```bash
   pip install -r requirements.txt
   ```

## Usage

To convert currencies, run the following command:

```bash
python currency.py
```

Follow the prompts to enter the source currency, target currency, and amount to convert.

## Testing

To run the tests, use the following command:

```bash
pytest
```

## Contributing

Contributions to this project are welcome. Feel free to open issues and submit pull requests.

## License

This project is licensed under the [MIT License](LICENSE).

## Acknowledgements

This project was developed as part of a school assignment in studying DevOps and Python application development.