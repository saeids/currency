"""
Unit tests for currency conversion
"""

import unittest
from unittest import TestCase
from unittest.mock import patch
import requests
from io import StringIO

from currency import convert_currency

class TestCurrencyConverter(TestCase):
    @patch('builtins.input', side_effect=['USD', 'EUR', '100'])
    @patch('requests.get')
    def test_convert_currency(self, mock_requests_get, _):
        # Mocking the response from the API
        mock_response = {
            "rates": {
                "EUR": 0.89
            }
        }
        mock_requests_get.return_value.json.return_value = mock_response

        # Redirecting stdout to a StringIO object to capture the output
        output = StringIO()
        with patch('sys.stdout', new=output):
            convert_currency()

        # Asserting the expected output
        expected_output = "100 USD is 89.0 EUR\n"
        self.assertEqual(output.getvalue(), expected_output)

    @patch('builtins.input', side_effect=['InvalidCurrency', 'EUR', '100'])
    @patch('requests.get')
    def test_invalid_from_currency(self, mock_requests_get, _):
        # Testing with an invalid 'from' currency.
        # The API would normally return an error message. Here, we're mocking this behavior.
        mock_requests_get.return_value.raise_for_status.side_effect = requests.exceptions.HTTPError()

        with self.assertRaises(requests.exceptions.HTTPError):
            convert_currency()

    @patch('builtins.input', side_effect=['USD', 'InvalidCurrency', '100'])
    @patch('requests.get')
    def test_invalid_to_currency(self, mock_requests_get, _):
        # Testing with an invalid 'to' currency.
        # The API would normally return an error message. Here, we're mocking this behavior.
        mock_requests_get.return_value.raise_for_status.side_effect = requests.exceptions.HTTPError()

        with self.assertRaises(requests.exceptions.HTTPError):
            convert_currency()

    @patch('builtins.input', side_effect=['USD', 'EUR', 'invalid_amount'])
    def test_invalid_amount(self, _):
        # Testing with an invalid amount (not a number)
        # The input function would normally raise a ValueError. We're not mocking anything here.
        with self.assertRaises(ValueError):
            convert_currency()

    @patch('builtins.input', side_effect=['USD', 'EUR', '100'])
    @patch('requests.get')
    def test_api_down(self, mock_requests_get, _):
        # Testing what happens when the API is down.
        # We're mocking the behavior of requests.get when it can't establish a connection.
        mock_requests_get.side_effect = requests.exceptions.ConnectionError()

        with self.assertRaises(requests.exceptions.ConnectionError):
            convert_currency()


if __name__ == '__main__':
    unittest.main()
